// Define UI Variables
const form = document.querySelector("#task-form");
const tasklist = document.querySelector(".collection");
const clearBtn = document.querySelector(".clear-tasks");
const filter = document.querySelector("#filter");
const taskInput = document.querySelector("#task");

// Instead of expose this globally, we're gonna load all event listeners using a function
loadEventListeners();

function loadEventListeners() {
    // Add task event
    form.addEventListener("submit", addTask);
}

// Add Task Function
function addTask(e) {
    if (taskInput.value === "") {
        alert("Add a task");
    }

    // Create li element
    const li = document.createElement("li");
    // Add class
    li.className = "collection-item";
    // Create text node and append to li
    li.appendChild(document.createTextNode(taskInput.value));
    // Create new link element
    const link = document.createElement("a");
    // Add class
    link.className = "delete-item secondary-content";
    // Add icon HTML
    link.innerHTML = '<i class="fa fa-remove"></i>';
    // Append the link to the li
    li.appendChild(link);

    // Append the li to the ul
    tasklist.appendChild(li);

    // Clears the input
    taskInput.value= "";

    // Prevents the default form's behavior
    e.preventDefault();
}